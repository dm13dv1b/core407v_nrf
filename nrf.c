#include "ch.h"
#include "hal.h"
#include "nrf24l01.h"
#include <stdio.h>
#include <string.h>

BinarySemaphore nrf_Bsem;

void set_ce(void)
{
    palSetPad(GPIOD, 15);
}

void clear_ce(void)
{
    palClearPad(GPIOD, 15);
}

void nrf24l01_init(void)
{
    char tx_buff[2];
    char rx_buff[2];
    uint8_t echo = 0;
    tx_buff[1] = 0; // empty transmit buffer will return status here

    tx_buff[0] = (NRF24L01_CMD_WRITE | NRF24L01_AD_CONFIG | NRF24L01_DI_CONFIG_PWR_UP | NRF24L01_DI_CONFIG_CRCO | NRF24L01_DI_CONFIG_EN_CRC | NRF24L01_DI_CONFIG_MASK_MAX_RT | NRF24L01_DI_CONFIG_MASK_TX_DS | NRF24L01_DI_CONFIG_MASK_RX_DR);
    chBSemWait(&nrf_Bsem);
    clear_ce();
    chThdSleepMilliseconds(1);
    spiSelectI(&SPID1);
    spiExchange(&SPID1, 2, tx_buff, rx_buff);
    set_ce();
    chBSemSignal(&nrf_Bsem);

    tx_buff[0] =  (NRF24L01_CMD_WRITE | NRF24L01_AD_STATUS | NRF24L01_DI_STATUS_MAX_RT | NRF24L01_DI_STATUS_RX_DR | NRF24L01_DI_STATUS_TX_DS);
    chBSemWait(&nrf_Bsem);
    clear_ce();
    chThdSleepMicroseconds(1);
    spiSelectI(&SPID1); 
    spiExchange(&SPID1, 2, tx_buff, rx_buff);
    set_ce();
    chBSemSignal(&nrf_Bsem);

    echo = nrf_read_reg(NRF24L01_AD_STATUS);
    echo = nrf_read_reg(NRF24L01_AD_SETUP_AW);
}

uint8_t nrf24l01_config(void)
{
    char tx_buff[2];
    char rx_buff[2];
    tx_buff[0] = 0x0A;
    tx_buff[1] = 0x0A;
    rx_buff[0] = 0;
    rx_buff[1] = 0;
    chBSemWait(&nrf_Bsem);
    clear_ce();
    chThdSleepMicroseconds(1);
    spiSelectI(&SPID1);
    spiExchange(&SPID1, 5, tx_buff, rx_buff);
    set_ce();
    chBSemSignal(&nrf_Bsem);
    return rx_buff[1];
}

uint8_t nrf_read_reg(uint8_t addr)
    {
        char tx_buff[2];
        char rx_buff[2];
        tx_buff[0] = (NRF24L01_CMD_READ | addr);
	tx_buff[1] = 0x0F;
	memcpy(rx_buff, "     ", 5);
	chBSemWait(&nrf_Bsem);
	clear_ce();
	chThdSleepMilliseconds(1);
	spiSelectI(&SPID1);
	//spiStartSendI(&SPID1, 1, tx_buff);
	spiStartExchangeI(&SPID1, 2, tx_buff, rx_buff);
	set_ce();
	chBSemSignal(&nrf_Bsem);
	return (uint8_t)rx_buff[1];
    }

uint8_t nrf_alive(void)
{
    uint8_t aw;
    aw = nrf_read_reg(NRF24L01_AD_SETUP_AW);

    return ((aw & 0xFC) == 0x00 && (aw & 0x03) != 0x00);
}

void nrf_Bsem_init(void)
{
  /*
   * Initialize semaphore.
   */
  chBSemInit(&nrf_Bsem, 1);
  chBSemSignal(&nrf_Bsem);
}

