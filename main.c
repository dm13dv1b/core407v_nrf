/* Local Variables: */
/* mode: C */
/* compile-command: "make -j 4" */
/* End: */

/* Multi line comment
   ESC j will make a new line
   ALT ; will make a comment block
   M-x comment-box make a comment box
*/

/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

//CE pin PD15

/***************/
/* Comment box */
/***************/

#include "ch.h"
#include "hal.h"
#include "shell.h"
#include "chprintf.h"
#include <stdlib.h>
#include "usbcfg.h"
#include "nrf24l01.h"

static void spicb(SPIDriver *spip);

/* Virtual serial port over USB.*/
SerialUSBDriver SDU1;

Thread *shelltp = NULL;

/*
 * SPI2 configuration structure.
 * Speed 21MHz, CPHA=0, CPOL=0, 16bits frames, MSb transmitted first.
 * The slave select line is the pin 4 on the port GPIOA.
 */
static const SPIConfig spi1cfg = {
    spicb,
    GPIOA,
    4,
    SPI_CR1_BR_0 | SPI_CR1_BR_1 | SPI_CR1_BR_2
};

/*===========================================================================*/
/* Command line related.                                                     */
/*===========================================================================*/

#define SHELL_WA_SIZE   THD_WA_SIZE(2048) /* shell size */

static void cmd_mem(BaseSequentialStream *chp, int argc, char *argv[]) {
  size_t n, size;

  (void)argv;
  if (argc > 0) {
    chprintf(chp, "Usage: mem\r\n");
    return;
  }
  n = chHeapStatus(NULL, &size);
  chprintf(chp, "core free memory : %u bytes\r\n", chCoreStatus());
  chprintf(chp, "heap fragments   : %u\r\n", n);
  chprintf(chp, "heap free total  : %u bytes\r\n", size);
}

static void cmd_nrf_config(BaseSequentialStream *chp, int argc, char *argv[]) {

  (void)argv;
  uint8_t aw;

  if (argc > 0) {
    chprintf(chp, "Usage: nrf_config\r\n");
    return;
  }
  aw = nrf24l01_config();
  if (aw > 0) {
      chprintf(chp, "NRF returned non zero value\r\n");
  }
}

static void cmd_nrf_init(BaseSequentialStream *chp, int argc, char *argv[]) {

  (void)argv;
  if (argc > 0) {
    chprintf(chp, "Usage: nrf_status\r\n");
    return;
  }
  nrf24l01_init();
}

static void cmd_threads(BaseSequentialStream *chp, int argc, char *argv[]) {
  static const char *states[] = {THD_STATE_NAMES};
  Thread *tp;

  (void)argv;
  if (argc > 0) {
    chprintf(chp, "Usage: threads\r\n");
    return;
  }
  chprintf(chp, "%10s %10s %10s %6s %6s %11s %7s\r\n", "name", "add", "stack", "prio", "refs", "state", "time");

  tp = chRegFirstThread();

  do {
      chprintf(chp, "%10s %.10lx %.10lx %6lu %6lu %11s %7lu\r\n",
               (uint32_t)tp->p_name, (uint32_t)tp, (uint32_t)tp->p_ctx.r13,
               (uint32_t)tp->p_prio, (uint32_t)(tp->p_refs - 1),
               states[tp->p_state], (uint32_t)tp->p_time);
    tp = chRegNextThread(tp);
  } while (tp != NULL);
}

static const ShellCommand commands[] = {
  {"mem", cmd_mem},
  {"threads", cmd_threads},
  {"nrf_config", cmd_nrf_config},
  {"nrf_init", cmd_nrf_init},
  {NULL, NULL}
};

static const ShellConfig shell_cfg1 = {
  (BaseSequentialStream *)&SDU1,
  commands
};

/*
 * SPI end transfer callback.
 */
static void spicb(SPIDriver *spip) {

  /* On transfer end just releases the slave select line.*/
  chSysLockFromIsr();
  spiUnselectI(spip);
  chSysUnlockFromIsr();
  set_ce();
}

/*
 * This is a periodic thread that does absolutely nothing except flashing
 * a LED.
 */
/***********************************************/
/* static WORKING_AREA(waThread1, 128);	       */
/* static msg_t Thread1(void *arg) {	       */
/*  					       */
/*   (void)arg;				       */
/*   uint8_t _alive = 0;		       */
/* 					       */
/*   chRegSetThreadName("nrf_alive");	       */
/*   while (TRUE) {			       */
/*     palClearPad(GPIOD, 14);		       */
/*     _alive = nrf_alive();		       */
/* 					       */
/*     if (_alive > 0 )			       */
/* 	{				       */
/* 	    palSetPad(GPIOD, 14);	       */
/* 	}				       */
/* 					       */
/*     chThdSleepMilliseconds(100);	       */
/*   }					       */
/*   return 0;				       */
/* }					       */
/***********************************************/

/*
 * This is a periodic thread that does absolutely nothing except flashing
 * a LED.
 */
static WORKING_AREA(waThread2, 128);
static msg_t alive(void *arg) {
 
  (void)arg;

  chRegSetThreadName("alive");
  while (TRUE) {
      palSetPad(GPIOD, 14);
      chThdSleepMilliseconds(500);
      palClearPad(GPIOD, 14);
      chThdSleepMilliseconds(500);
  }
  return 0;
}

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Activates the serial driver 2 using the driver default configuration.
   * PA2(TX) and PA3(RX) are routed to USART2.
   */
  sdStart(&SD2, NULL);
  palSetPadMode(GPIOA, 2, PAL_MODE_ALTERNATE(7));
  palSetPadMode(GPIOA, 3, PAL_MODE_ALTERNATE(7));

  /*
   * Initializes the SPI driver 1. The SPI1 signals are routed as follow:
   * PA4 - NSS.
   * PA5 - SCK.
   * PA6 - MISO.
   * PA7 - MOSI.
   */
  spiStart(&SPID1, &spi1cfg);
  palSetPad(GPIOA, 4);
  palSetPadMode(GPIOA, 4, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);           /* NSS.     */
  palSetPadMode(GPIOB, 3, PAL_MODE_ALTERNATE(5) | PAL_STM32_OSPEED_HIGHEST);           /* SCK.     */
  palSetPadMode(GPIOA, 6, PAL_MODE_ALTERNATE(5) | PAL_STM32_OSPEED_HIGHEST);           /* MISO.    */
  palSetPadMode(GPIOA, 7, PAL_MODE_ALTERNATE(5) | PAL_STM32_OSPEED_HIGHEST);           /* MOSI.    */
  palClearPad(GPIOA, 4);
  chThdSleepMilliseconds(100);

  /*
   * Activates the serial driver 2 using the driver default configuration.
   * PA2(TX) and PA3(RX) are routed to USART2.
   */
  sdStart(&SD2, NULL);
  palSetPadMode(GPIOA, 2, PAL_MODE_ALTERNATE(7));
  palSetPadMode(GPIOA, 3, PAL_MODE_ALTERNATE(7));

  /*
   * CE pin for nrf24l01
   */
  palSetPadMode(GPIOD, 15, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);
  palSetPadMode(GPIOD, 14, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);
  palClearPad(GPIOD, 14);

  /*
   * Shell manager initialization.
   */
  shellInit();

  /*
   * Initializes a serial-over-USB CDC driver.
   */
  sduObjectInit(&SDU1);
  sduStart(&SDU1, &serusbcfg);
  
  /*
   * Activates the USB driver and then the USB bus pull-up on D+.
   * Note, a delay is inserted in order to not have to disconnect the cable
   * after a reset.
   */
  usbDisconnectBus(serusbcfg.usbp);
  chThdSleepMilliseconds(1000);
  usbStart(serusbcfg.usbp, &usbcfg);
  usbConnectBus(serusbcfg.usbp);

  shelltp = shellCreate(&shell_cfg1, SHELL_WA_SIZE, NORMALPRIO);

  /*
   * Creates the example thread.
   */
  //chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
  chThdCreateStatic(waThread2, sizeof(waThread2), NORMALPRIO, alive, NULL);
 
  nrf_Bsem_init();
  /*
   * Normal main() thread activity, in this demo it just performs
   * a shell respawn upon its termination.
   */
  while (TRUE) {
    if (!shelltp) {
      if (SDU1.config->usbp->state == USB_ACTIVE) {
        /* Spawns a new shell.*/
        shelltp = shellCreate(&shell_cfg1, SHELL_WA_SIZE, NORMALPRIO);
      }
    }
    else {
      /* If the previous shell exited.*/
      if (chThdTerminated(shelltp)) {
        /* Recovers memory of the previous shell.*/
        chThdRelease(shelltp);
        shelltp = NULL;
      }
    }
    chThdSleepMilliseconds(500);
  }
}
